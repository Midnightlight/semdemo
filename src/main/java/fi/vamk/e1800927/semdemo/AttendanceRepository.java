package fi.vamk.e1800927.semdemo;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);

    // public Attendance findByDay(java.util.Date day);
}
