package fi.vamk.e1800927.semdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/")
    public String swagger() {
        return "redirect:/swagger-ui.html";
        // ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
    }

    @RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
        // ServerResponse.temporaryRedirect(URI.create("swagger-ui.html")).build());
    }
}

